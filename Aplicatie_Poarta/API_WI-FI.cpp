#include <WiFi.h>
#include <ArduinoJson.h>
#include <HTTPClient.h>

#define LED 2
#define ESP_D 12
#define ESP_I 13

const char* ssid = "Wifi";
const char* wifipassword =  "12345678";
const char* url =  "http://34.70.249.187:5010/api/TemperatureMeasurement";
const char* urlLogin = "http://34.70.249.187:5010/api/Auth/Login";
char * username = "test2@mailinator.com";
char * password = "Parola01@";
String json = "{ \"email\": \"test2@mailinator.com\", \"password\": \"Parola01@\"}";
char token[400] = "Bearer ";



bool isOn = false;
bool oldState = false;
void setup() {
  Serial.begin(115200);
  delay(1000);
  pinMode(LED, OUTPUT);
  pinMode(ESP_D, OUTPUT);
  pinMode(ESP_I, OUTPUT);
    
  digitalWrite(ESP_D, LOW);
  digitalWrite(ESP_I, LOW);
  delay(100);
  setup_wifi();
  login();

}

void setup_wifi() {
  delay(10);

  Serial.print("Connecting to ");
  Serial.println(ssid);
  delay(500);
  WiFi.begin(ssid, wifipassword);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED, HIGH);
    delay(250);
    digitalWrite(LED, LOW);
    delay(250);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}




char * login() {

  HTTPClient http;
  StaticJsonDocument<1000> doc;
  String payload;
  http.begin(urlLogin); //Specify the URL
  http.addHeader("accept", "text/plain");
  http.addHeader("Content-Type", "application/json-patch+json");
  int httpResponseCode = http.POST(json);
  if (httpResponseCode > 0) { //Check for the returning code

    payload = http.getString();
    Serial.println(httpResponseCode);
    Serial.println(payload);

  }

  else {
    Serial.println("Error on HTTP request");
  }

  http.end(); //Free the resources

  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return "deserialize error";
  }
  strcat(token, doc["token"]);

}
void getInfo() {
  HTTPClient http;
  StaticJsonDocument<1000> doc;

  http.begin("http://34.70.249.187:5010/api/Door/GetDoorStatus");
  http.addHeader("accept", "text/plain");
  http.addHeader("Authorization", token);
  int httpCode = http.GET();
  if (httpCode > 0) { //Check for the returning code

    String payload = http.getString();
    DeserializationError error = deserializeJson(doc, payload);

    Serial.println(httpCode);
    Serial.println(payload);

    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      http.end();
      return ;
    }

    
    int n = payload.length(); 
  
    // declaring character array 
    char char_array[n + 1]; 
     strcpy(char_array, payload.c_str()); 
    if(strcmp(char_array,"false")==0)
        isOn=false;
    else if (strcmp(char_array,"true")==0)
        isOn=true;
   
   

  }

  else {
    Serial.println("Error on HTTP request");
  }
 

  http.end();
}


void loop() {
  
 

  if ((WiFi.status() == WL_CONNECTED) && strcmp(token, "Bearer ") != 0) { //Check the current connection status

    getInfo();

  } else {
    Serial.println("Login...");
    login();
  }
  if (isOn==true && oldState==false){
    digitalWrite(LED, HIGH);
    digitalWrite(ESP_D, HIGH);
    delay(100);
    digitalWrite(ESP_D, LOW);

  }
  if (isOn==false && oldState==true){
    digitalWrite(LED, LOW);
    digitalWrite(ESP_I, HIGH);
    delay(100);
    digitalWrite(ESP_I, LOW);

  }
  oldState=isOn;
 
  delay(3000);




}